package ru.iteco.taskmanager.service;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ru.iteco.taskmanager.entity.Domain;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.entity.User;

public class DomainService {

	public void jaxbSaveXml(@Nullable List<User> userList, @Nullable List<Project> projectList, @Nullable List<Task> taskList) {
		try {
			final Domain domain = new Domain(userList, projectList, taskList);
			@Nullable
			final JAXBContext jaxbContext = JAXBContext.newInstance(domain.getClass());
			@Nullable
			final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(domain, new File("jaxb.xml"));
			jaxbMarshaller.marshal(domain, System.out);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	@Nullable
	public Domain jaxbLoadXml() {
		@Nullable
		Domain domain = new Domain();
		try {
			@Nullable
			final JAXBContext jaxbContext = JAXBContext.newInstance(domain.getClass());

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			domain = (Domain) jaxbUnmarshaller.unmarshal(new File("jaxb.xml"));
		  } catch (JAXBException e) {
			e.printStackTrace();
		  }
		return domain;
	}
	
	public void jaxbSaveJson(@Nullable List<User> userList, @Nullable List<Project> projectList, @Nullable List<Task> taskList) throws Exception {
		@Nullable
		final Domain domain = new Domain(userList, projectList, taskList);
		final Map<String, Object> properties = new HashMap<>();
		final Class[] classes = new Class[] {Domain.class};
		System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
		properties.put("eclipselink-media-type", "application/json");
		@Nullable
		final JAXBContext jaxbContext = JAXBContext.newInstance(classes, properties);
		@Nullable
		final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(domain, new File("jaxb.json"));
	    jaxbMarshaller.marshal(domain, System.out);
	}
	
	@Nullable
	public Domain jaxbLoadJson() throws Exception {
		final Map<String, Object> properties = new HashMap<>();
		final Class[] classes = new Class[] {Domain.class};
		System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
		properties.put("eclipselink-media-type", "application/json");
		@Nullable
		final JAXBContext jaxbContext = JAXBContext.newInstance(classes, properties);
		@Nullable
		final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (Domain) jaxbUnmarshaller.unmarshal(new File("jaxb.json"));
	}
	
	public void jacksonSaveXml(@Nullable List<User> userList, @Nullable List<Project> projectList, @Nullable List<Task> taskList) throws Exception {
		@Nullable
		final Domain domain = new Domain(userList, projectList, taskList);
		@NotNull
		final ObjectMapper mapper = new XmlMapper();
		@NotNull
		final String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
		FileWriter writer = new FileWriter("jackson.xml");
		writer.write(result);
		writer.flush();
	}
	
	@Nullable
	public Domain jacksonLoadXml() throws Exception {
		XmlMapper xmlMapper = new XmlMapper();
        String readContent = new String(Files.readAllBytes(Paths.get("jackson.xml")));
        Domain domain = xmlMapper.readValue(readContent, Domain.class);
        return domain;
	}
	
	public void jacksonSaveJson(@Nullable List<User> userList, @Nullable List<Project> projectList, @Nullable List<Task> taskList) throws Exception {
		@Nullable
		final Domain domain = new Domain(userList, projectList, taskList);
		@NotNull
		final ObjectMapper mapper = new ObjectMapper();
		mapper.writerWithDefaultPrettyPrinter().writeValue(new File("jackson.json"), domain);
	}
	
	@Nullable
	public Domain jacksonLoadJson() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Domain domain = mapper.readValue(new File("jackson.json"), Domain.class);
		return domain;
	}
}
