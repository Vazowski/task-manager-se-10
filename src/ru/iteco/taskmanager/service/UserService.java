package ru.iteco.taskmanager.service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.repository.UserRepository;

public final class UserService extends AbstractService implements IUserService {

	@NotNull
	private UserRepository userRepository;
	@NotNull
	private StringBuilder currentUser = new StringBuilder("root");
	
	public UserService() {
		userRepository = new UserRepository();
	}
	
	@Nullable
	public User get(@NotNull String login) {
		return userRepository.get(login);
	}
	
	public void set(@Nullable final List<User> list) {
		if (list == null) return;
		userRepository.clear();
		for (final User user : list) {
			userRepository.merge(user.getLogin(), user.getPasswordHash(), user.getRoleType(), user.getUuid());
		}
	}
	
	@Nullable
	public User findByLogin(@NotNull final String login) {
		@Nullable
		final List<User> users = userRepository.findAll();
		if (users.size() == 0)
			return null;
		for (final User user : users) {
			if (login.equals(user.getLogin()))
				return user;
		}
		return null;
	}
	
	@Nullable
	public List<User> findAll() {
		return userRepository.findAll();
	}
	
	public void merge(@NotNull final String login, @NotNull final String passwordHash, @NotNull final RoleType roleType) {
		@NotNull
		final String uuid = UUID.randomUUID().toString();
		if (!login.equals(null) && !passwordHash.equals(null) && !roleType.equals(null))
			userRepository.merge(login, passwordHash, roleType, uuid);
	}
	
	public void merge(@NotNull final String login, @NotNull final String passwordHash) {
		@NotNull
		final String uuid = UUID.randomUUID().toString();
		if (!login.equals(null) && !passwordHash.equals(null))
			userRepository.merge(login, passwordHash, RoleType.USER, uuid);
	}
	
	@Nullable
	public String getCurrent() {
		return currentUser.toString();
	}
	
	public void setCurrent(@NotNull final String login) {
		if (!login.equals(null))
			this.currentUser = new StringBuilder(login);
	}
	
	@Nullable
	public String getPassword(@NotNull final String password) {
		if (password.equals(null))
				return null;
		final StringBuffer sb = new StringBuffer();
		try {
			final MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes(StandardCharsets.UTF_8));
			byte byteData[] = md.digest();
			for (int i = 0; i < byteData.length; i++)
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
