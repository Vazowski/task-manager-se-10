package ru.iteco.taskmanager.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;

public final class TerminalService extends AbstractService {

	@NotNull
	private Map<String, AbstractCommand> commands;
	
	public TerminalService() {
		commands = new LinkedHashMap<String, AbstractCommand>();
	}
	
	public void put(@NotNull final String command, @NotNull final AbstractCommand abstractCommand) {
		commands.put(command, abstractCommand);
	}
	
	@Nullable
	public AbstractCommand get(@NotNull final String command) {
		return commands.get(command);
	}
	
	@Nullable
	public Map<String, AbstractCommand> getCommands() {
		return commands;
	}
}
