package ru.iteco.taskmanager.bootstrap;

import java.util.Scanner;
import java.util.Set;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.Setter;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.service.TerminalService;
import ru.iteco.taskmanager.service.DomainService;
import ru.iteco.taskmanager.service.ProjectService;
import ru.iteco.taskmanager.service.TaskService;
import ru.iteco.taskmanager.service.UserService;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

	@NotNull
	private Scanner scanner;
	@NotNull
	private String inputCommand;
	@NotNull
	private static final String DEFAULT_PASSWORD = "1234";
	@NotNull
	private final TerminalService terminalService = new TerminalService();
	@NotNull
	private final ProjectService projectService = new ProjectService(new ProjectRepository(), new TaskRepository());
	@NotNull
	private final TaskService taskService = new TaskService(new TaskRepository());
	@NotNull
	private final UserService userService = new UserService();
	@NotNull
	private final DomainService domainService = new DomainService();
	
	public void init(@Nullable final Set<Class<? extends AbstractCommand>> classes) {
		scanner = new Scanner(System.in);
		try {
			createDefaultUsers();
			registerCommand(classes);

			System.out.println("Enter command (type help for more information)");

			while (true) {
				System.out.print("> ");
				inputCommand = scanner.nextLine();
				if (!terminalService.getCommands().containsKey(inputCommand)) {
					System.out.println("Command doesn't exist");
				} else {
					terminalService.get(inputCommand).execute();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void registerCommand(@Nullable final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        if (classes == null) return;
        for (@Nullable Class clazz : classes) {
            if (clazz == null || !AbstractCommand.class.isAssignableFrom(clazz)) continue;
            @NotNull AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setServiceLocator(this);
            registerCommand(command);
        }
    }

    private void registerCommand(@Nullable final AbstractCommand command) {
        if (command == null) return;
        terminalService.put(command.command(), command);
    }

	
	private void createDefaultUsers() {
		userService.merge("root", userService.getPassword(DEFAULT_PASSWORD), RoleType.ADMIN);
		userService.merge("guest", userService.getPassword(DEFAULT_PASSWORD), RoleType.USER);
	}
	
	
}
