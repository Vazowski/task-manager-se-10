package ru.iteco.taskmanager.command.project.create;

import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public final class ProjectMergeCommand extends AbstractCommand{

	private String inputName, inputDescription, uuid, dateBegin, dateEnd;

	@Override
	public String command() {
		return "project-merge";
	}

	@Override
	public String description() {
		return "  -  merge project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		System.out.print("Description of project: ");
		inputDescription = scanner.nextLine();
		System.out.print("Date of begining project: ");
		dateBegin = scanner.nextLine();
		System.out.print("Date of ending project: ");
		dateEnd = scanner.nextLine();
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull 
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable 
		final List<Project> tempList = serviceLocator.getProjectService().findAll();
		@Nullable
		final String projectUuid;
		if (tempList != null) {
			for (final Project project : tempList) {
				if (project.getName().equals(inputName)) {
					if (!project.getOwnerId().equals(userUuid)) {
						System.out.println("Project create other user");
						return;
					}
					projectUuid = project.getUuid();
					serviceLocator.getProjectService().merge(inputName, inputDescription, projectUuid, project.getOwnerId(), dateBegin, dateEnd);
					System.out.println("Done");
					return;
				}
			}
		}
		uuid = UUID.randomUUID().toString();
		serviceLocator.getProjectService().merge(inputName, inputDescription, uuid, serviceLocator.getUserService().get(currentUser).getUuid(), dateBegin, dateEnd);
		System.out.println("Done");
	}
}
