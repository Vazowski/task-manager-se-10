package ru.iteco.taskmanager.command.project.update;

import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public final class ProjectPersistCommand extends AbstractCommand{

	private String inputName, inputDescription, uuid, dateBegin, dateEnd;

	@Override
	public String command() {
		return "project-persist";
	}

	@Override
	public String description() {
		return "  -  persist project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		@Nullable 
		final List<Project> tempList = serviceLocator.getProjectService().findAll();
		@NotNull 
		final String currentUser = serviceLocator.getUserService().getCurrent();
		if (tempList.size() > 0) {
			for (final Project project : tempList) {
				if (project.getName().equals(inputName)) {
					throw new Exception("Project with same name already exist");
				} 
			}
		}
		System.out.print("Description of project: ");
		inputDescription = scanner.nextLine();
		System.out.print("Date of begining project: ");
		dateBegin = scanner.nextLine();
		System.out.print("Date of ending project: ");
		dateEnd = scanner.nextLine();
		uuid = UUID.randomUUID().toString();
		serviceLocator.getProjectService().merge(inputName, inputDescription, uuid, serviceLocator.getUserService().get(currentUser).getUuid(), dateBegin, dateEnd);
		System.out.println("Done");
	}
}
