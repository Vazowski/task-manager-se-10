package ru.iteco.taskmanager.command.project.remove;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public final class ProjectRemoveCommand extends AbstractCommand {

	private String inputProjectName;
	
	@Override
	public String command() {
		return "project-remove";
	}

	@Override
	public String description() {
		return "  -  remove one project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		@NotNull 
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable 
		final List<Project> tempList = serviceLocator.getProjectService().findAll(userUuid);
		@Nullable 
		String projectUuid;
		for (final Project project : tempList) {
			if (project.getName().equals(inputProjectName)) {
				projectUuid = project.getUuid();
				@NotNull
				final Project tempProject = serviceLocator.getProjectService().findByUuid(projectUuid, userUuid); 
				if (tempProject != null) {
					System.out.println("Project doesn't exist");
					break;
				}
				serviceLocator.getProjectService().remove(projectUuid, userUuid);
				System.out.println("Done");
			}
		}
	}
}
