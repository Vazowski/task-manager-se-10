package ru.iteco.taskmanager.command.project.remove;

import ru.iteco.taskmanager.command.AbstractCommand;

public final class ProjectRemoveAllCommand extends AbstractCommand{

	@Override
	public String command() {
		return "project-remove";
	}

	@Override
	public String description() {
		return "  -  remove one project";
	}

	@Override
	public void execute() throws Exception {
		serviceLocator.getProjectService().removeAll();
		System.out.println("Done");
	}
}
