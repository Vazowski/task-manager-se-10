package ru.iteco.taskmanager.command.project.find;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public class ProjectFindAllByNamePartCommand extends AbstractCommand {

	@NotNull
	private String partOfName; 
	
	@Override
	public @NotNull String command() {
		return "project-find-all-name-part";
	}

	@Override
	public @NotNull String description() {
		return "  -  find all project by part of name";
	}

	@Override
	public void execute() throws Exception {
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		System.out.print("Part of name project: ");
		partOfName = scanner.nextLine();
		@Nullable
		final List<Project> tempList = serviceLocator.getProjectService().findAllByPartName(userUuid, partOfName);
		if (tempList.size() == 0) {
			System.out.println("Empty");
			return;
		}
		for (int i = 0, j = 1; i < tempList.size(); i++) {
			if (tempList.get(i).getOwnerId().equals(userUuid)) {
				System.out.println("[Project " + (j++) + "]");
				System.out.println(serviceLocator.getProjectService().findByUuid(tempList.get(i).getUuid()));
			}
		}
	}

}
