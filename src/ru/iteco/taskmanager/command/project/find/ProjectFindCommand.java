package ru.iteco.taskmanager.command.project.find;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public final class ProjectFindCommand extends AbstractCommand {
	
	private String inputName;
	
	@Override
	public String command() {
		return "project-find";
	}

	@Override
	public String description() {
		return "  -  find one project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		@NotNull 
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull 
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable 
		final List<Project> tempList = serviceLocator.getProjectService().findAll(userUuid);
		@Nullable 
		final String projectUuid;
		for (Project project : tempList) {
			if (project.getName().equals(inputName)) {
				projectUuid = project.getUuid();
				@NotNull 
				final Project tempProject = serviceLocator.getProjectService().findByUuid(projectUuid, userUuid); 
				if (tempProject == null) 
					throw new Exception("No project with same name");
				System.out.println(tempProject);
				break;
			}
		}
	}
}
