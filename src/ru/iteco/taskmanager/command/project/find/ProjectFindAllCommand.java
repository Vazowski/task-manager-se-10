package ru.iteco.taskmanager.command.project.find;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public final class ProjectFindAllCommand extends AbstractCommand {

	@Override
	public String command() {
		return "project-find-all";
	}

	@Override
	public String description() {
		return "  -  find all project";
	}

	@Override
	public void execute() throws Exception {
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable
		final List<Project> tempList = serviceLocator.getProjectService().findAll();
		if (tempList == null) return;
		for (int i = 0, j = 1; i < tempList.size(); i++) {
			if (tempList.get(i).getOwnerId().equals(userUuid)) {
				System.out.println("[Project " + (j++) + "]");
				System.out.println(serviceLocator.getProjectService().findByUuid(tempList.get(i).getUuid()));
			}
		}
	}
}