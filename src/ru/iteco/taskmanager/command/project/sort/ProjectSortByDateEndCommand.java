package ru.iteco.taskmanager.command.project.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public class ProjectSortByDateEndCommand extends AbstractCommand {

	@Override
	public String command() {
		return "project-sort-date-end";
	}

	@Override
	public String description() {
		return "  -  find all project and sort them by date end";
	}

	@Override
	public void execute() throws Exception {
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable
		final List<Project> tempList = serviceLocator.getProjectService().findAll(userUuid);
		
		@NotNull
		final Comparator<Project> compareByDateEnd = (Project o1, Project o2) -> o1.getDateEnd().compareTo(o2.getDateEnd() );
		Collections.sort(tempList, compareByDateEnd);
		
		for (int i = 0, j = 1; i < tempList.size(); i++) {
			if (tempList.get(i).getOwnerId().equals(userUuid)) {
				System.out.println("[Project " + (j++) + "]");
				System.out.println(serviceLocator.getProjectService().findByUuid(tempList.get(i).getUuid()));
			}
		}
	}

}
