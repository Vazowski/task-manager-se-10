package ru.iteco.taskmanager.command.task.update;

import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;

public final class TaskMergeCommand extends AbstractCommand {

	private String inputProjectName, inputName, inputDescription, uuid, findedUuid, projectUuid, dateBegin, dateEnd;
	
	@Override
	public String command() {
		return "task-merge";
	}

	@Override
	public String description() {
		return "  -  merge task";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		@NotNull 
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull 
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable 
		Project tempProject = serviceLocator.getProjectService().findByName(inputProjectName, userUuid);
		if (tempProject == null) {
			System.out.println("Project doesn't exist");
			return;
		} 
		projectUuid = tempProject.getUuid();
		System.out.print("Name of task: ");
		inputName = scanner.nextLine();
		System.out.print("Description of task: ");
		inputDescription = scanner.nextLine();
		System.out.print("Date of begining project: ");
		dateBegin = scanner.nextLine();
		System.out.print("Date of ending project: ");
		dateEnd = scanner.nextLine();
		@Nullable
		final Task tempTask = serviceLocator.getTaskService().findByName(inputName, userUuid);
		if (tempTask == null) {
			uuid = UUID.randomUUID().toString();
			serviceLocator.getTaskService().merge(inputName, inputDescription, uuid, projectUuid, userUuid, dateBegin, dateEnd);
			System.out.println("Done");
			return;
		}
		if (tempTask.getOwnerId() == userUuid) {
			System.out.println("Task create other user");
			return;
		}
		findedUuid = tempTask.getUuid();
		serviceLocator.getTaskService().merge(inputName, inputDescription, findedUuid, projectUuid, userUuid, dateBegin, dateEnd);
		System.out.println("Done");
	}
}
