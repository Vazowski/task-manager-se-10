package ru.iteco.taskmanager.command.task.find;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;

public final class TaskFindCommand extends AbstractCommand {

	private String inputProjectName, inputName;
	
	@Override
	public String command() {
		return "task-find";
	}

	@Override
	public String description() {
		return "  -  find one task in project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		@NotNull 
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull 
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable 
		Project tempProject = serviceLocator.getProjectService().findByName(inputProjectName, userUuid);
		if (tempProject == null) {
			System.out.println("Project doesn't exist");
			return;
		} 
		System.out.print("Name of task: ");
		inputName = scanner.nextLine();
		@Nullable 
		final Task tempTask = serviceLocator.getTaskService().findByName(inputName, userUuid); 
		if (tempTask == null) 
			throw new Exception("No task with same name");
		System.out.println(tempTask);
	}
}
