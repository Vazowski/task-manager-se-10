package ru.iteco.taskmanager.command.task.find;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Task;

public class TaskFindAllByDescriptionPartCommand extends AbstractCommand {

	@NotNull
	private String partOfDescription; 
	
	@Override
	public @NotNull String command() {
		return "task-find-all-description-part";
	}

	@Override
	public @NotNull String description() {
		return "  -  find all task by part of description";
	}

	@Override
	public void execute() throws Exception {
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		System.out.print("Part of description task: ");
		partOfDescription = scanner.nextLine();
		@Nullable
		final List<Task> tempList = serviceLocator.getTaskService().findAllByPartDescription(userUuid, partOfDescription);
		if (tempList.size() == 0) {
			System.out.println("Empty");
			return;
		}
		for (int i = 0, j = 1; i < tempList.size(); i++) {
			if (tempList.get(i).getOwnerId().equals(userUuid)) {
				System.out.println("[Task " + (j++) + "]");
				System.out.println(serviceLocator.getTaskService().findByUuid(tempList.get(i).getUuid()));
			}
		}
	}

}
