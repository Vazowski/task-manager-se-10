package ru.iteco.taskmanager.command.task.find;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;

public final class TaskFindAllCommand extends AbstractCommand {
	
	private String inputProjectName, projectUuid;
	
	@Override
	public String command() {
		return "task-find-all";
	}

	@Override
	public String description() {
		return "  -  find all task in project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull 
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable 
		Project tempProject = serviceLocator.getProjectService().findByName(inputProjectName, userUuid);
		if (tempProject == null) {
			System.out.println("Project doesn't exist");
			return;
		}
		projectUuid = tempProject.getUuid();
		@Nullable 
		final List<Task> tempList = serviceLocator.getTaskService().findAll(projectUuid, userUuid);
		if (tempList.size() == 0) {
			System.out.println("Empty");
			return;
		}
		for (int i = 0; i < tempList.size(); i++) {
			System.out.println("[Task " + (i + 1) + "]");
			System.out.println(tempList.get(i));
		}
	}
}
