package ru.iteco.taskmanager.command.task.create;

import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;

public final class TaskPersistCommand extends AbstractCommand {

	private String inputProjectName, inputName, inputDescription, uuid, projectUuid, dateBegin, dateEnd;
	
	@Override
	public String command() {
		return "task-persist";
	}

	@Override
	public String description() {
		return "  -  persist task";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@Nullable
		Project tempProject = serviceLocator.getProjectService().findByName(inputProjectName);
		if (tempProject == null) {
			System.out.println("Project doesn't exist");
			return;
		} 
		projectUuid = tempProject.getUuid();
		System.out.print("Name of task: ");
		inputName = scanner.nextLine();
		@Nullable 
		final Task tempTask = serviceLocator.getTaskService().findByName(inputName);
		if (tempTask != null) {
			throw new Exception("Task with same name already exist");
		}
		System.out.print("Description of task: ");
		inputDescription = scanner.nextLine();
		System.out.print("Date of begining project: ");
		dateBegin = scanner.nextLine();
		System.out.print("Date of ending project: ");
		dateEnd = scanner.nextLine();
		uuid = UUID.randomUUID().toString();
		serviceLocator.getTaskService().merge(inputName, inputDescription, uuid, projectUuid, serviceLocator.getUserService().get(currentUser).getUuid(), dateBegin, dateEnd);
		System.out.println("Done");
	}
}
