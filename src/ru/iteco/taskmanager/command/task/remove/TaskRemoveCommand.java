package ru.iteco.taskmanager.command.task.remove;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;

public final class TaskRemoveCommand extends AbstractCommand {

	private String inputProjectName, inputName;
	
	@Override
	public String command() {
		return "task-remove";
	}

	@Override
	public String description() {
		return "  -  remove task from project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull 
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable
		Project tempProject = serviceLocator.getProjectService().findByName(inputProjectName, userUuid);
		if (tempProject == null) {
			System.out.println("Project doesn't exist");
			return;
		} 
		System.out.print("Name of task: ");
		inputName = scanner.nextLine();
		@Nullable 
		final Task tempTask = serviceLocator.getTaskService().findByName(inputName, userUuid);
		if (tempTask != null) {
			System.out.println("Task doesn't exist");
			return;
		}
		serviceLocator.getTaskService().remove(inputName);
		System.out.println("Done");
	}
}
