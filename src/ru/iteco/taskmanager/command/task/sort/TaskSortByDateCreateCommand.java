package ru.iteco.taskmanager.command.task.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Task;

public class TaskSortByDateCreateCommand extends AbstractCommand {

	@Override
	public String command() {
		return "task-sort-date-create";
	}

	@Override
	public String description() {
		return "  -  find all task and sort them by date create";
	}

	@Override
	public void execute() throws Exception {
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable
		final List<Task> tempList = serviceLocator.getTaskService().findAll(userUuid);
		
		@NotNull
		final Comparator<Task> compareByDateCreate = (Task o1, Task o2) -> o1.getDateCreated().compareTo(o2.getDateCreated() );
		Collections.sort(tempList, compareByDateCreate);
		
		for (int i = 0, j = 1; i < tempList.size(); i++) {
			if (tempList.get(i).getOwnerId().equals(userUuid)) {
				System.out.println("[Task " + (j++) + "]");
				System.out.println(serviceLocator.getTaskService().findByUuid(tempList.get(i).getUuid()));
			}
		}
	}

}
