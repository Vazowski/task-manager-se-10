package ru.iteco.taskmanager.command.user;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.command.AbstractCommand;

public final class UserCreateCommand extends AbstractCommand {
	
	@Override
	public String command() {
		return "user-create";
	}

	@Override
	public String description() {
		return "  -  create new user";
	}

	@Override
	public void execute() throws Exception {
		while (true) {
			System.out.print("Username: ");
			@NotNull 
			final String login = scanner.nextLine();
			if (serviceLocator.getUserService().findByLogin(login) != null) {
				System.out.println("Username exist");
				break;
			} 
			System.out.print("Password: ");
			@NotNull 
			final String password = scanner.nextLine();
			serviceLocator.getUserService().merge(login, serviceLocator.getUserService().getPassword(password));
			System.out.println("Done");
			break;
		}
	}

	@NotNull
	@Override
	public List<String> getRoles() {
		return new ArrayList<String>() { {
			add("Administrator");
		}
		};
	}
}
