package ru.iteco.taskmanager.command.serialize.save;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.entity.User;

public class DataToJsonJacksonCommand extends AbstractCommand {

	@Override
	public String command() {
		return "jackson-to-json";
	}

	@Override
	public String description() {
		return "  -  save data to json via jackson";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final List<User> userList = serviceLocator.getUserService().findAll();
		if (userList == null) return;
		@Nullable
		final List<Project> projectList = serviceLocator.getProjectService().findAll();
		@Nullable
		final List<Task> taskList = serviceLocator.getTaskService().findAll();
		
		serviceLocator.getDomainService().jacksonSaveJson(userList, projectList, taskList);
		System.out.println("Done");
	}

}
