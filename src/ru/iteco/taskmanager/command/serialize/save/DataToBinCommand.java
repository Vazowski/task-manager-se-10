package ru.iteco.taskmanager.command.serialize.save;


import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.AbstractEntity;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.entity.User;

public class DataToBinCommand extends AbstractCommand {

	@Override
	public @NotNull String command() {
		return "data-to-bin";
	}

	@Override
	public @NotNull String description() {
		return "  -  save data to binary file";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final List<User> userList = serviceLocator.getUserService().findAll();
		if (userList != null) {
			saveData(userList, "user");
		}
		
		@Nullable
		final List<Project> projectList = serviceLocator.getProjectService().findAll();
		if (projectList != null) 
			saveData(projectList, "project");
		
		@Nullable
		final List<Task> taskList = serviceLocator.getTaskService().findAll();
		if (taskList != null) {
			saveData(projectList, "task");
		}
		
		System.out.println("Done");
	}

	private void saveData(@NotNull final List<? extends AbstractEntity> list, @NotNull final String fileName) throws Exception {
		@NotNull 
        final File file = new File(fileName + ".bin");
        @NotNull 
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull 
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(list);
        objectOutputStream.close();
        fileOutputStream.close();
    }
}
