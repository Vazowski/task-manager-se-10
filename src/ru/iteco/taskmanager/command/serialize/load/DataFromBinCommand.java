package ru.iteco.taskmanager.command.serialize.load;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.entity.User;

public class DataFromBinCommand extends AbstractCommand {

	@Nullable
	private String uuid;
	
	@Override
	public @NotNull String command() {
		return "data-from-bin";
	}

	@Override
	public @NotNull String description() {
		return "  -  load data from binary file";
	}

	@Override
	public void execute() throws Exception {
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull 
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@NotNull
		String projectUuid = "";
		@Nullable
		@SuppressWarnings("unchecked")
		final List<User> userList = (List<User>) loadData("user");
		if (userList != null) {
			for (final User user : userList) {
				if (user == null)
					continue;
				uuid = UUID.randomUUID().toString();
				serviceLocator.getUserService().merge(user.getLogin(), user.getPasswordHash(), user.getRoleType());
			}
		}
		
		@Nullable
		@SuppressWarnings("unchecked")
		final List<Project> projectList = (List<Project>) loadData("project");
		if (projectList != null) {
			serviceLocator.getProjectService().removeAll();
			for (final Project project : projectList) {
				if (project == null)
					continue;
				uuid = UUID.randomUUID().toString();
				serviceLocator.getProjectService().merge(project.getName(), project.getDescription(), uuid, userUuid,
						project.getDateBegin(), project.getDateEnd());
				projectUuid = project.getUuid();
			}
		}
		@Nullable
		@SuppressWarnings("unchecked")
		final List<Task> taskList = (List<Task>) loadData("task");
		if (taskList != null) {
			for (final Task task : taskList) {
				if (task == null)
					continue;
				uuid = UUID.randomUUID().toString();
				serviceLocator.getTaskService().merge(task.getName(), task.getDescription(), uuid, userUuid,
						projectUuid, task.getDateBegin(), task.getDateEnd());
			}
		}
		
		serviceLocator.getTerminalService().get("logout").execute();
		System.out.println("Done");
	}

	
	@Nullable
	private List loadData(@NotNull final String fileName) throws IOException, ClassNotFoundException {
		File f = new File(fileName + ".bin");
		if(!f.exists()) {
			System.out.println("File " + fileName + ".bin not found");
			return null;
		}
		
		@NotNull
		final File file = new File(fileName + ".bin");
        @NotNull
        final FileInputStream fileInputStream = new FileInputStream(file);
        @NotNull
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable
        final List list = (ArrayList) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
		return list;
	}
}
