package ru.iteco.taskmanager.command.serialize.load;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Domain;

public class DataFromJsonJacksonCommand extends AbstractCommand {

	@Override
	public String command() {
		return "jackson-from-json";
	}

	@Override
	public String description() {
		return "  -  load data from json via jackson";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Domain domain = serviceLocator.getDomainService().jacksonLoadJson();
		
		serviceLocator.getUserService().set(domain.getUserList());
		serviceLocator.getProjectService().set(domain.getProjectList());
		serviceLocator.getTaskService().set(domain.getTaskList());
		
		serviceLocator.getTerminalService().get("logout").execute();
		System.out.println("Done");
	}

}
