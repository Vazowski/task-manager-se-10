package ru.iteco.taskmanager.command.serialize.load;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Domain;

public class DataFromJsonJaxbCommand extends AbstractCommand {

	@Override
	public String command() {
		return "jaxb-from-json";
	}

	@Override
	public String description() {
		return "  -  load data from json via jaxb";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Domain domain = serviceLocator.getDomainService().jaxbLoadJson();
		
		serviceLocator.getUserService().set(domain.getUserList());
		serviceLocator.getProjectService().set(domain.getProjectList());
		serviceLocator.getTaskService().set(domain.getTaskList());
		
		serviceLocator.getTerminalService().get("logout").execute();
		System.out.println("Done");
	}

}
