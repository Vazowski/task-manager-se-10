package ru.iteco.taskmanager.command.serialize.load;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Domain;

public class DataFromXmlJacksonCommand extends AbstractCommand {

	@Override
	public @NotNull String command() {
		return "jackson-from-xml";
	}

	@Override
	public @NotNull String description() {
		return "  -  load data from xml via jackson";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Domain domain = serviceLocator.getDomainService().jacksonLoadXml();
		
		serviceLocator.getUserService().set(domain.getUserList());
		serviceLocator.getProjectService().set(domain.getProjectList());
		serviceLocator.getTaskService().set(domain.getTaskList());
		
		serviceLocator.getTerminalService().get("logout").execute();
		System.out.println("Done");
	}

}
