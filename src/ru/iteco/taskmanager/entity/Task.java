package ru.iteco.taskmanager.entity;

import java.io.Serializable;
import java.util.Date;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;
import ru.iteco.taskmanager.enumerate.ReadinessStatus;

@Getter 
@Setter
public final class Task extends AbstractEntity implements Serializable {

	@NotNull
	private String ownerId;
	@NotNull
	private String projectUUID;
	@NotNull
	private String name;
	@NotNull
	private String description;
	@NotNull
	private String dateCreated;
	@NotNull
	private String dateBegin;
	@NotNull
	private String dateEnd;
	@NotNull
	private String type;
	@NotNull
	private ReadinessStatus readinessStatus;
	
	public Task() {
    	
    }
    
    public Task(@NotNull String name, @NotNull String description, @NotNull String uuid, @NotNull String projectUuid, @NotNull String ownerId, @NotNull final String dateBegin, @NotNull final String dateEnd) {
    	this.ownerId = ownerId;
    	this.uuid = uuid;
    	this.projectUUID = projectUuid;
    	this.name = name;
    	this.description = description;
    	this.dateCreated = dateFormat.format(new Date());
    	this.dateBegin = dateBegin;
    	this.dateEnd = dateEnd;
    	this.type = "Task";
    	this.readinessStatus = ReadinessStatus.PLANNED;
    }
	
	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateCreated: " + this.dateCreated + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid + "\nprojectUUID: " + this.projectUUID;
	}
}
