package ru.iteco.taskmanager.api.service;

import java.util.List;

import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.entity.User;

public interface IUserService {

	User get(String login);
	User findByLogin(final String login);
	List<User> findAll();
	void merge(final String login, final String passwordHash, final RoleType roleType);
	void merge(final String login, final String passwordHash);
	String getCurrent();
	void setCurrent(final String login);
	String getPassword(final String password);
}
