# Task Manager SE-10

This is another implementation of the Task Manager project

## Software

Work OS and clean hands, actual JDK

## Stack

Java 1.7, Maven

## Deploy

mvn clean install

## Launch

java -jar "task-manager-se-10.jar"

## Contacts

Denis Ermakov
